package Battleship;

import java.util.*;

public abstract class Ship {

	private Vector<Marker> coordinates = new Vector<Marker>();
	
	public enum Direction {Vertical, Horizontal};
	private Direction direction = Direction.Vertical;
	
	
	public Ship(int len){
		
		coordinates.setSize(len);
	}
	public int getLength(){
		return coordinates.size();
	}
	public Vector<Marker> getCoordinates(){
		return coordinates;
	}
	protected void setCoordinates(Vector<Marker>coordinates){
		this.coordinates = coordinates;
	}
	public void setCoordinates(Coordinate coord, Direction dir){
		
		int length = this.getLength();
		coordinates = new Vector<Marker>();
		int x0 = coord.getX();
		int y0 = coord.getY();
		
		for(int i = 0; i < length; i++){
			coordinates.add(new Marker(x0,y0));
			if(dir == Direction.Horizontal){
				x0++;
			}
			else{
				y0++;
			}
		}
	}
	public Direction getDirection(){
		return direction;
	}
	public void setDirection(Direction direction){
		this.direction = direction;
	}
	
}
