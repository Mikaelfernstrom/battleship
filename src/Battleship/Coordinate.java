package Battleship;

public class Coordinate {

	private int x, y;

	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Coordinate c){
		
		return(c.x == this.x && c.y == this.y);
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
