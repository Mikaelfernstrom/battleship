package Battleship;

public class Board {
	
	private int empty;
	private int miss;
	private int hit;
	private int width = 10;
	private int height = 10;
	private int [][] board;

	
	public Board (){
		this.board = new int[this.width][this.height];
		this.initBoard();
		this.showBoard();
	}
	public void initBoard(){
		for(int i = 0; i < this.width; i++){
			for(int j = 0; j < this.height; j++){
				board[i][j]= empty;
			}
		}
	}
	
	public void showBoard(){
		System.out.println("\tA \tB \tC \tD \tE \tF \tG \tH \tI \tJ");
		
		for(int i = 0; i < this.width; i++){
			System.out.print((i+1) + "");
			
			for(int j = 0; j < this.height; j++){
				 if(board[i][j]== empty){
				System.out.print("\t"+"~");
				 }else if(board[i][j]==miss){
                 System.out.print("\t"+"*");
             }else if(board[i][j]==hit){
                 System.out.print("\t"+"X");
             }
         }
         System.out.println();
     }

 }
}